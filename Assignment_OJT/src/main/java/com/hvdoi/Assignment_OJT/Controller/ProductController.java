package com.hvdoi.Assignment_OJT.Controller;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.hvdoi.Assignment_OJT.Repository.ProductRepository;

@Controller
public class ProductController {
	@Autowired
	private ProductRepository productRepnsitory;
	
	@GetMapping({"/product", "/", ""})
	public String index(Model model) {
		
		model.addAttribute("productsss", productRepnsitory.findAll());
		return "product/index";
		
	}
	
	@GetMapping({"/viewDetail/{viewId}"})
	public String ViewDetail(@PathVariable(value = "viewId") Long viewId, Model model) {
		model.addAttribute("viewbyID", productRepnsitory.findById(viewId));
		
		return "product/viewDetail";
		
	}
	@GetMapping("/addProduct")
	public String addProduct() {
		
		return "product/addProduct";
	}
	
	@GetMapping("/viewCart")
	public String viewCart() {
		
		return "product/cart";
	}
	
	@GetMapping("/Login")
	public String Login() {
		
		return "product/login";
	}
}

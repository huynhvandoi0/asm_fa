package com.hvdoi.Assignment_OJT.Repository;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hvdoi.Assignment_OJT.Model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
}

package com.hvdoi.Assignment_OJT.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.relational.core.mapping.Column;

import com.fasterxml.jackson.annotation.JacksonInject.Value;

@Entity
@Table(name="Product")
public class Product {
	public Product(){}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Long ProductID;
	
	@Column(value = "ProductName")
	private String ProductName;
    
	@Column(value = "UnitPrice")
	private int UnitPrice;
	
	@Column(value = "UnitInStock")
    private int UnitInStock;
    
	@Column(value = "Discription")
    private String Discription;
	
	@Column(value = "Manufacturer")
    private String Manufacturer;
	
	@Column(value = "Category")
    private String Category;
    
	@Column(value = "ConditionID")
    private int ConditionID;

	@Column(value = "Img")
	private String Img;
    
	public Long getProductID() {
		return ProductID;
	}
	public void setProductID(Long productID) {
		ProductID = productID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public int getUnitPrice() {
		return UnitPrice;
	}
	public void setUnitPrice(int unitPrice) {
		UnitPrice = unitPrice;
	}
	public int getUnitInStock() {
		return UnitInStock;
	}
	public void setUnitInStock(int unitInStock) {
		UnitInStock = unitInStock;
	}
	public String getDiscription() {
		return Discription;
	}
	public void setDiscription(String discription) {
		Discription = discription;
	}
	public String getManufacturer() {
		return Manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		Manufacturer = manufacturer;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public int getConditionID() {
		return ConditionID;
	}
	public void setConditionID(int conditionID) {
		ConditionID = conditionID;
	}
	public String getImg() {
		return Img;
	}
	public void setImg(String img) {
		Img = img;
	}
	@Override
	public String toString() {
		return "Product [ProductID=" + ProductID + ", ProductName=" + ProductName + ", UnitPrice=" + UnitPrice
				+ ", UnitInStock=" + UnitInStock + ", Discription=" + Discription + ", Manufacturer=" + Manufacturer
				+ ", Category=" + Category + ", ConditionID=" + ConditionID + ", Img=" + Img + "]";
	}
    
    
    
}

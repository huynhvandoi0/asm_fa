package com.hvdoi.Assignment_OJT;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hvdoi.Assignment_OJT.Model.Product;


@SpringBootApplication
public class AssignmentOjtApplication{
	

	public static void main(String[] args) {
		SpringApplication.run(AssignmentOjtApplication.class, args);
	}

}

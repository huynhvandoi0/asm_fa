create table Permission (
	PermissionID int identity(1,1) primary key,
	Name nvarchar(50)
);
go

INSERT [dbo].[Permission] ([Name]) VALUES (N'Admin');
INSERT [dbo].[Permission] ([Name]) VALUES (N'Customer');

create table Account (
	AccountID int identity(1,1) primary key,
	Username varchar(50) unique,
	[Password] nvarchar(100),
	PermissionID int,
	foreign key (PermissionID) references Permission(PermissionID)
);
go
/*************them*************/
INSERT [dbo].[Account] ([Username], [Password], [PermissionID])
VALUES ('admin1', N'123456', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID]) 
VALUES ('admin2', N'123456', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID]) 
VALUES ('user1', N'123456', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID]) 
VALUES ('user2', N'123456', N'1');
INSERT [dbo].[Account] ([Username], [Password], [PermissionID]) 
VALUES ('user3', N'123456', N'1');
create table Condition (
	ConditionID int identity(1,1) primary key,
	Name nvarchar(50)
);
go
INSERT [dbo].[Condition] ([Name]) VALUES (N'New');
INSERT [dbo].[Condition] ([Name]) VALUES (N'Old');
INSERT [dbo].[Condition] ([Name]) VALUES (N'Refurbished');

create table Product (
	ProductID int identity(1,1) primary key,
	ProductName varchar(50) unique,
	UnitPrice int,
	UnitInStock int,
	Disciption varchar(500),
	Manufacturer varchar(50),
	Category varchar(50),
	ConditionID int,
	Img nvarchar(500),
	foreign key (ConditionID) references Condition(ConditionID)
);

go
/***************************/
INSERT [dbo].[Product] ([Img], [ProductName], [UnitPrice], [UnitInStock], [Disciption], [Manufacturer], [Category], [ConditionID])
VALUES ('https://th.bing.com/th/id/R.3db16b60e57e72efa4c14e56ee5a637e?rik=gRvVKjTTQoYq3A&pid=ImgRaw&r=0', N'Iphone 11', N'100', N'10', N'Beautiful and Strong', N'Apple', N'Smartphone', N'2');
INSERT [dbo].[Product] ([Img], [ProductName], [UnitPrice], [UnitInStock], [Disciption], [Manufacturer], [Category], [ConditionID])
VALUES ('https://th.bing.com/th/id/R.8783a54fa189065080cffa7f2efd190a?rik=7c81HC3SZMeDsA&pid=ImgRaw&r=0', N'Iphone 12', N'150', N'10', N'Beautiful and Strong', N'Apple', N'Smartphone', N'2');
INSERT [dbo].[Product] ([Img], [ProductName], [UnitPrice], [UnitInStock], [Disciption], [Manufacturer], [Category], [ConditionID])
VALUES ('https://th.bing.com/th/id/R.bf51b9708827581163936e51f07bdbbb?rik=s5fksay%2frVCzNA&pid=ImgRaw&r=0', N'Iphone 13', N'200', N'10', N'Beautiful and Strong', N'Apple', N'Smartphone', N'1');
INSERT [dbo].[Product] ([Img], [ProductName], [UnitPrice], [UnitInStock], [Disciption], [Manufacturer], [Category], [ConditionID])
VALUES ('https://th.bing.com/th/id/R.bf51b9708827581163936e51f07bdbbb?rik=s5fksay%2frVCzNA&pid=ImgRaw&r=0', N'Iphone 14', N'250', N'10', N'Beautiful and Strong', N'Apple', N'Smartphone', N'1');

create table [Order] (
	OrderID int identity(1,1) primary key,
	AccountID int,
	ProductID int,
	ProductName varchar(50),
	Quantity int,
	UnitPrice int,
	Price int,
	foreign key (AccountID) references Account(AccountID),
    foreign key (ProductID) references Product(ProductID)
);
go
INSERT [dbo].[Order] ([AccountID], [ProductID], [ProductName], [Quantity], [UnitPrice], [Price])
VALUES ('1', N'1', N'Iphone 11', N'1', N'100', N'100');
INSERT [dbo].[Order] ([AccountID], [ProductID], [ProductName], [Quantity], [UnitPrice], [Price])
VALUES ('1', N'2', N'Iphone 12', N'1', N'100', N'100');
INSERT [dbo].[Order] ([AccountID], [ProductID], [ProductName], [Quantity], [UnitPrice], [Price])
VALUES ('2', N'3', N'Iphone 13', N'1', N'100', N'100');
INSERT [dbo].[Order] ([AccountID], [ProductID], [ProductName], [Quantity], [UnitPrice], [Price])
VALUES ('4', N'4', N'Iphone 14', N'1', N'100', N'100');
